import 'package:flutter/material.dart';
import 'package:flutter_application_1/data/datasources/trip_datasource.dart';
import 'package:flutter_application_1/data/repositories/trip_repositories.dart';
import 'package:flutter_application_1/domain/models/trip_model.dart';
import 'package:flutter_application_1/presentation/widgets/bus_card.dart';
import 'package:flutter_application_1/presentation/widgets/checkbox_custom.dart';
import 'package:flutter_application_1/presentation/widgets/elevated_button.dart';
import 'package:flutter_application_1/presentation/widgets/search_fild.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../blocs/trip_bloc.dart';

import 'package:dio/dio.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  bool _isButtonPressed = false;

  @override
  Widget build(BuildContext context) {
    final tripDataSource = TripDataSource(Dio());
    final tripRepository = TripRepository(tripDataSource);
    final tripBloc = TripBloc(tripRepository);

    return BlocProvider(
      create: (context) => tripBloc,
      child: DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            title: Text('Поиск поездок'),
            backgroundColor: Colors.green,
          ),
          body: SingleChildScrollView(
            child: Column(
              children: [
                _buildTabBar(),
                _buildSearchFields(),
                _buildDriverInfo(
                    trip: Trip(
                        departureCity: 'Казань',
                        destinationCity: 'Уфа',
                        date: '22.03.2024')),
                SizedBox(height: 15.0),
                BlocBuilder<TripBloc, List<Trip>>(
                  builder: (context, trips) {
                    if (trips.isEmpty) {
                      return Container(); // Returning an empty container
                    } else {
                      return _buildTripList(trips);
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildTabBar() {
    return TabBar(
      tabs: [
        Tab(
          text: 'Пассажир',
        ),
        Tab(
          text: 'Водитель',
        ),
      ],
      labelColor: Colors.green,
      unselectedLabelColor: Colors.black,
      indicatorColor: Colors.green, // Добавляем цвет внизу при выбранном табе
    );
  }

  Widget _buildSearchFields() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Row(
            children: [
              Icon(Icons.location_on),
              SizedBox(width: 8.0),
              Expanded(
                child: SearchField(
                  placeholder: 'Откуда: Город отбытия',
                  suffixIcon: Icons.directions_bus,
                ),
              ),
            ],
          ),
          SizedBox(height: 8.0),
          Row(
            children: [
              Icon(Icons.directions_bus),
              SizedBox(width: 8.0),
              Expanded(
                child: SearchField(
                  placeholder: 'Куда: Город прибытия',
                  suffixIcon: Icons.directions_bus,
                ),
              ),
            ],
          ),
          SizedBox(height: 15.0),
          CustomCheckbox(text: "Передать посылку"),
          SizedBox(height: 20.0),
          ColorElevatedButton(
            onPressed: () {},
            child: Text('Поиск'),
          ),
        ],
      ),
    );
  }

  Widget _buildTripList(List<Trip> trips) {
    return RefreshIndicator(
      onRefresh: () async {
        // Обработчик обновления списка
      },
      child: ListView.builder(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemCount: trips.length,
        itemBuilder: (context, index) {
          return _buildDriverInfo(trip: trips[index]);
        },
      ),
    );
  }

Widget _buildDriverInfo({required Trip trip}) {
  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: Container(
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey),
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(
                      Icons.person,
                      size: 40.0,
                      color: Colors.black,
                    ),
                  ),
                  SizedBox(width: 8.0),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Иван Иванов',
                        style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              Row(
                children: [
                  Text(
                    'Toyota Camry',
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(width: 8.0),
                  Text(
                    '1500 руб.',
                    style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  IconButton(
                    icon: Icon(Icons.star_border),
                    onPressed: () {
                      // Добавить водителя в избранное
                    },
                  ),
                ],
              ),
            ],
          ),
          SizedBox(height: 5.0),
          Text(
            'Откуда: ${trip.departureCity}',
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.bold,
            ),
          ),
          Text(
            'Куда: ${trip.destinationCity}',
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(height: 5.0),
          Text(
            'Дата и время отправления: ${trip.date}',
            style: TextStyle(fontSize: 12),
          ),
          Text(
            'Дата и время прибытия: ${trip.date}',
            style: TextStyle(fontSize: 12),
          ),
        ],
      ),
    ),
  );
}
}
