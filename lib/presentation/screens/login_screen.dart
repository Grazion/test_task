import 'package:flutter/material.dart';
import 'package:flutter_application_1/presentation/screens/main_screen.dart';
import 'package:flutter_application_1/presentation/widgets/elevated_button.dart';
import 'package:flutter_application_1/presentation/widgets/text_button.dart';

import 'package:url_launcher/url_launcher.dart';


class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Login'),
        backgroundColor: Colors.green, 
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ColorElevatedButton(
              onPressed: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(builder: (context) => MainScreen()),
                );
              },
              child: Text('Get Started', style: TextStyle(color: Colors.white)),
            ),
            SizedBox(height: 16), // Промежуток между кнопками и текстовыми кнопками
            ColorTextButton(
              onPressed: () {
                // Navigate to Terms and Conditions or Privacy Policy website
              },
              child: Text('Terms and Conditions', style: TextStyle(color: Colors.green)),
            ),
            SizedBox(height: 8), // Промежуток между кнопками
            ColorTextButton(
              onPressed: () {
                // Navigate to Terms and Conditions or Privacy Policy website
              },
              child: Text('Privacy Policy', style: TextStyle(color: Colors.green)),
            ),
          ],
        ),
      ),
    );
  }
}
