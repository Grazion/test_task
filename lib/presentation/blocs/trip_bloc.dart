import 'package:flutter_application_1/data/repositories/trip_repositories.dart';
import 'package:flutter_application_1/domain/models/trip_model.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class TripBloc extends Cubit<List<Trip>> {
  final TripRepository _repository;

  TripBloc(this._repository) : super([]);

  Future<void> fetchTrips(
      String departureCity, String destinationCity, String date) async {
    try {
      final trips =
          await _repository.getTrips(departureCity, destinationCity, date);
      emit(trips);
    } catch (error) {
      print(error); // Добавляем логирование ошибок
    }
  }
}
