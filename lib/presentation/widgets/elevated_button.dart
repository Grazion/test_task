import 'package:flutter/material.dart';

class ColorElevatedButton extends StatelessWidget {
  final VoidCallback onPressed;
  final Widget child;

  const ColorElevatedButton({Key? key, required this.onPressed, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed,
      child: child,
      style: ElevatedButton.styleFrom(
        backgroundColor: Colors.green, // Зеленый цвет для кнопки
        padding: EdgeInsets.symmetric(horizontal: 24, vertical: 12), // Отступы кнопки
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)), // Закругленные углы
      ),
    );
  }
}