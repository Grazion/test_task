import 'package:flutter/material.dart';

class CustomCheckbox extends StatefulWidget {
  final String text;

  const CustomCheckbox({Key? key, required this.text}) : super(key: key);

  @override
  _CustomCheckboxState createState() => _CustomCheckboxState();
}

class _CustomCheckboxState extends State<CustomCheckbox> {
  bool isChecked = false;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        setState(() {
          isChecked = !isChecked;
        });
      },
      child: Row(
        children: [
          Container(
            width: 24.0,
            height: 24.0,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(color: isChecked ? Colors.blue : Colors.grey),
            ),
            child: isChecked
                ? Icon(Icons.check, size: 20.0, color: Colors.blue)
                : Container(),
          ),
          SizedBox(width: 8.0),
          Text(
            widget.text,
            style: TextStyle(
              fontSize: 16.0,
              color: isChecked ? Colors.blue : Colors.black,
            ),
          ),
        ],
      ),
    );
  }
}