import 'package:flutter/material.dart';

class SearchField extends StatelessWidget {
  final String? label;
  final String? placeholder;
  final IconData? suffixIcon;

  const SearchField({
    Key? key,
    this.label,
    this.placeholder,
    this.suffixIcon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: [
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12.0), // Овальная форма
                border: Border.all(color: Colors.grey), // Белая обводка
              ),
              child: Padding(
                padding: const EdgeInsets.only(left: 8.0), // Отступ слева
                child: TextFormField(
                  decoration: InputDecoration(
                    labelText: label,
                    hintText: placeholder,
                    border: InputBorder.none, // Убираем обводку при выделении
                    suffixIcon: suffixIcon != null
                        ? IconTheme(
                            data: IconThemeData(
                                color: Colors.grey), // Серый цвет для иконки
                            child: Icon(suffixIcon),
                          )
                        : null,
                    hintStyle:
                        TextStyle(color: Colors.grey), // Цвет placeholder'а
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}