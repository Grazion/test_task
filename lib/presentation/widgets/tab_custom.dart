import 'package:flutter/material.dart';

class CustomTab extends StatelessWidget {
  final String text;
  final bool isCurrent;

  const CustomTab({
    required this.text,
    required this.isCurrent,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: isCurrent ? Colors.green : Colors.white,
      child: Center(
        child: Text(
          text,
          style: TextStyle(
            color: Colors.black,
          ),
        ),
      ),
    );
  }
}

class CustomTabBar extends StatelessWidget {
  final List<String> tabTitles;
  final int currentIndex;

  const CustomTabBar({
    required this.tabTitles,
    required this.currentIndex,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TabBar(
      tabs: tabTitles.map((title) {
        final index = tabTitles.indexOf(title);
        return CustomTab(
          text: title,
          isCurrent: index == currentIndex,
        );
      }).toList(),
      labelColor: Colors.green, // Цвет выбранной вкладки
      unselectedLabelColor: Colors.black, // Цвет не выбранных вкладок
    );
  }
}