import 'package:flutter/material.dart';
import 'package:flutter_application_1/domain/models/trip_model.dart';

class BusCard extends StatelessWidget {
  final Trip trip;

  const BusCard({required this.trip});

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        title: Text('${trip.departureCity} - ${trip.destinationCity}'),
        subtitle: Text('Date: ${trip.date}'),
      ),
    );
  }
}
