import 'package:flutter/material.dart';

class ColorTextButton extends StatelessWidget {
  final VoidCallback onPressed;
  final Widget child;

  const ColorTextButton({Key? key, required this.onPressed, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: onPressed,
      child: child,
      style: TextButton.styleFrom(
        foregroundColor: Colors.green, padding: EdgeInsets.symmetric(horizontal: 24, vertical: 12), // Отступы кнопки
      ),
    );
  }
}