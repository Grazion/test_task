import 'package:flutter/material.dart';

class ButtonThemeSettings {
  static ElevatedButtonThemeData elevatedButtonTheme() {
    return ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
        foregroundColor: Colors.white,
        backgroundColor: Colors.green, // Цвет текста для поднятой кнопки
      ),
    );
  }

  static TextButtonThemeData textButtonTheme() {
    return TextButtonThemeData(
      style: TextButton.styleFrom(
        foregroundColor: Colors.green, // Цвет текста для текстовой кнопки
      ),
    );
  }
}
