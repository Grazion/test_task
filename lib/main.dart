import 'package:flutter/material.dart';
import 'package:flutter_application_1/data/datasources/trip_datasource.dart';
import 'package:flutter_application_1/data/repositories/trip_repositories.dart';
import 'package:flutter_application_1/presentation/blocs/trip_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:dio/dio.dart';
import 'package:flutter_application_1/presentation/screens/splash_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final tripDataSource = TripDataSource(Dio());
    final tripRepository = TripRepository(tripDataSource);

    return BlocProvider(
      create: (context) => TripBloc(tripRepository),
      child: MaterialApp(
        title: 'Flutter app',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: SplashScreen(),
      ),
    );
  }
}
