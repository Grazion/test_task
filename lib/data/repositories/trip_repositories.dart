import 'package:flutter_application_1/data/datasources/trip_datasource.dart';
import 'package:flutter_application_1/domain/models/trip_model.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:dio/dio.dart';

class TripRepository {
  final TripDataSource _dataSource;

  TripRepository(this._dataSource);

  Future<List<Trip>> getTrips(String departureCity, String destinationCity, String date) async {
    return _dataSource.fetchTrips(departureCity, destinationCity, date);
  }
}
