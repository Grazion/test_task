import 'package:dio/dio.dart';
import 'package:flutter_application_1/domain/models/trip_model.dart';

class TripDataSource {
  final Dio _dio;

  TripDataSource(this._dio);

  Future<List<Trip>> fetchTrips(
      String departureCity, String destinationCity, String date) async {
    try {
      final response = await _dio.get(
          'https://bibiptrip.com/api/avibus/search_trips_cities/?departure_city=${Uri.encodeComponent(departureCity)}&destination_city=${Uri.encodeComponent(destinationCity)}&date=${Uri.encodeComponent(date)}');

      // Парсинг данных из ответа сервера и создание списка объектов Trip
      List<Trip> trips =
          (response.data as List).map((json) => Trip.fromJson(json)).toList();

      return trips;
    } catch (error) {
      throw Exception('Failed to fetch trips: $error');
    }
  }
}
