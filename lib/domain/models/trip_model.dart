class Trip {
  final String departureCity;
  final String destinationCity;
  final String date;

  Trip({
    required this.departureCity,
    required this.destinationCity,
    required this.date,
  });

  factory Trip.fromJson(Map<String, dynamic> json) {
    return Trip(
      departureCity: json['departureCity'],
      destinationCity: json['destinationCity'],
      date: json['date'],
    );
  }
}